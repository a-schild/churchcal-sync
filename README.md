# ChurchTools Wordpress Plugin
> Dieses Projekt stellt ein Plugin für Wordpress zur Verfügung. Es ermöglicht die Einbindung von ChurchTools Terminen.

Es ist ein Fork des Plugins aus folgendem ChurchTool Forum:

https://forum.church.tools/topic/2467/gibt-es-erfahrungen-mit-einbindung-in-wordpress/37

Copyright 2016 - 2019, Kevin Hermann (kevin.hermann@online.de) and the churchcal-sync contributors.

Copyright 2017 - 2019, Thomas Stadelmann ( thomas.stadelmann@wave.ch) and the churchcal-sync contributors.

## Requirements
You need to have Bootstrap Modal Plugin installed: https://de.wordpress.org/plugins/bootstrap-modals/

### Bug Issue
My Popups are closing immediately after opening.   
Solution:   
Comment line 44 in the modal.php of the plugin (~/wp-content/plugins/bootstrap-modals/modal.php):
```
	//wp_register_script( 'modaljs' , plugins_url( '/js/bootstrap.min.js',  __FILE__), array( 'jquery' ), '3.3.7', true );
	wp_register_style( 'modalcss' , plugins_url( '/css/bootstrap.css',  __FILE__), '' , '3.3.7', 'all' );
	wp_register_style( 'custommodalcss' , plugins_url( '/css/custommodal.css',  __FILE__), '' , '3.3.7', 'all' );
```


## Usage
### General Configuration
In the admin-panel, go to `Settings > ChurchTools Importer` and set your ChurchTools URL, the [categories](#obtaining-category-ids) you want to be displayed and the maximum number of events to be shown.
### Basic Usage
Just add the shortcode `[churchtools_cal]` to an element in your Homepage, and you will see your ChurchTools-events with the settings you have applied [above](#general-configuration).

You can also show only one event with `[churchtools_evt]`.
### Extended Usage
If you don't want to display the same calendar with exactly the same look and the same events over and over again, you can customize the displayed calendar by adding attributes to the shortcode:

For example, if you'd like to show only one specific category on one subpage, simply add `categories="_1, 2, 3_"`, with a comma-seperated list of the category-IDs that you want to be used.

You can combine as much attributes as you want, all available attributes are listed below:

#### Attributes for both shortcodes
| Keyword        | Value type               | Default value                    | Description                                                                                                                                |
| ---            | ---                      | ---                              | ---                                                                                                                                        |
| categories     | comma-separated integers | _Given in general configuration_ | List of Category-IDs whose events should be displayed.                                                                                     |
| show\_category | boolean                  | false                            | Specifies if the name of the category, to which the event belongs, should be displayed. Currently only applicable to the `default` layout. |
| offset         | integer                  | 0                                | Sets the offset at which the events should start (how many events should be skipped).                                                      |

#### Attributes for `churchtools_cal`
| Keyword            | Value type | Default value                    | Description                                                                                |
| ---                | ---        | ---                              | ---                                                                                        |
| events\_per\_page  | integer    | _Given in general configuration_ | Number of events to be shown.                                                              |
| style              | string     | default                          | The layout-style of the event list. Available layouts are `default`, `block`, `detail-block` and `short`.           |
| show\_update\_info | boolean    | false                            | Specifies if the time at which the internal database was last updated should be displayed. |

#### Attributes for `churchtools_evt`
| Keyword            | Value type | Default value                    | Description                                                                                                                                                              |
| ---                | ---        | ---                              | ---                                                                                                                                                                      |
| component          | string     | event                            | The component of the event you want to be displayed. Possible values are: `event` (displays whole event); `title`; `description`; `link`, `startdate`, `enddate`, `date`, `modal` (see comment)|
| modal_id           | string     | none                             | Adds a Popup Link with the given ID                                                                                            |

If you use modal_id, you have to create a second shortcode like `[churchtools_evt categories="18" offset=1 component="modal" modal_id="1"]` with the same modal_id. This shortcode must be placed outside of any gutenberg block.   
If you choose to display the link, you have to define the text around which the link is wrapped. You do so by adding a closing shortcode tag after the normal shortcode and writing your text between them. You can also add other shortcodes inside, but if you want to use the `[churchtools_evt]` shortcode, you have to replace it with the alias `[churchtools_sub_evt]` so that the shortcode-parser of wordpress doesn't get confused.

**Example:**
```
[churchtools_evt component="link"]
Link text goes here
[/churchtools_evt]
```

**Screenshot with Popup:**   
![Screenshot mit Popup; Bild 0](/uploads/f073dbf9beda379afd765ccae5234f32/image.png)

### Accessing protected calendars
When you leave the user id and authentication token empty, the plugin has only access
to public CT calendars.

If you wish to also import non-public CT calendars, you need to specify the 
userID and authentication token of a user which has the read right on the specified
calendars


### Obtaining Category-IDs
I explain this procedure to you using the Firefox browser. However, the steps are similar on other browsers.

To get the IDs corresponding to your categories, you have to open a browser, navigate to your ChurchTools-Page.
Then open the Developer Tools of your browser (Typically with the Key `F12`) and switch to the `Networking` tab _(1)_, where you filter for `cause:xhr` _(2)_.
After that, you open the Calendar page _(3)_ and wait for the page to be loaded.

In the networking tab should now appear multiple requests. Select one of them and switch to the `Params` tab _(4)_. Now find the request in which the parameter `func` is `getMasterData` _(5)_.

![Anleitung zum finden der Kategorien; Bild 1](documentation/category-tutorial_1.jpg)

When you have found it, switch to the `Response` tab _(6)_ and find the element `category` _(7)_. The sub-elements now show you the Category-ID and the corresponding name _(8)_.

![Anleitung zum finden der Kategorien; Bild 2](documentation/category-tutorial_2.jpg)

## Release History

* v 2.0
    * Initial Release

## Meta

Gemeinschaft Immanuel Ravensburg – [Homepage](https://immanuel-online.de) – info@immanuel-online.de

Distributed under the GPL2 license. See [LICENSE](LICENSE) for more information.

[https://gitlab.com/immanuelrv/churchcal-sync](https://gitlab.com/immanuelrv/churchcal-sync)

## Contributing

1. Fork it (<https://gitlab.com/immanuelrv/churchcal-sync/fork>)
2. Create your feature branch (`git checkout -b master`)
3. Commit your changes (`git commit -am 'Add some changes'`)
4. Push to the branch (`git push origin master`)
5. Create a new Pull Request



